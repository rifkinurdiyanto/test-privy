resource "aws_eip" "privy-nat1" {
  vpc = true

  tags = {
    Name = "privy-nat1"
  }
}

resource "aws_eip" "privy-nat2" {
  vpc = true

  tags = {
    Name = "privy-nat2"
  }
}

resource "aws_nat_gateway" "privy-nat1" {
  allocation_id = aws_eip.privy-nat1.id
  subnet_id     = aws_subnet.privy-subnet-public1-ap-southeast-1a.id

  tags = {
    Name = "privy-nat1"
  }
}

resource "aws_nat_gateway" "privy-nat2" {
  allocation_id = aws_eip.privy-nat2.id
  subnet_id     = aws_subnet.privy-subnet-public2-ap-southeast-1b.id

  tags = {
    Name = "privy-nat2"
  }


  depends_on = [aws_internet_gateway.privy-igw]
}
