resource "aws_internet_gateway" "privy-igw" {
  vpc_id = aws_vpc.privy-test.id

  tags = {
    Name = "privy-igw"
  }
}