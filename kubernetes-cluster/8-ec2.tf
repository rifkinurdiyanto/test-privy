
resource "aws_instance" "privy-instance" {
  ami                         = "ami-04ff9e9b51c1f62ca"
  associate_public_ip_address = "true"
  disable_api_termination     = false
  key_name                    = "EC2Privy"
  instance_type               = "t3.medium"
  subnet_id                   = aws_subnet.privy-subnet-public1-ap-southeast-1a.id
  user_data                   = file("install.sh")

  root_block_device {
    volume_size           = 50
    volume_type           = "gp2"
    delete_on_termination = true
  }

  vpc_security_group_ids = [
    aws_security_group.privy-sg.id
  ]

  tags = {
    Name = "privy-test"
  }
}

resource "aws_security_group" "privy-sg" {
  name        = "privy"
  description = "Seurity Rule for Privy Test"
  vpc_id      = aws_vpc.privy-test.id

  ingress {
    description = "Allow Nginx HTTP request only from its VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.privy-test.cidr_block]
    #ipv6_cidr_blocks = [aws_vpc.privy-test.ipv6_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "privy"
  }
}