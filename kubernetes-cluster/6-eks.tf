resource "aws_iam_role" "EKSPrivy" {
  name = "eks-cluster-EKSPrivy"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "EKSPrivy-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.EKSPrivy.name
}

resource "aws_eks_cluster" "EKSPrivy" {
  name     = "EKSPrivy"
  role_arn = aws_iam_role.EKSPrivy.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.privy-subnet-private1-ap-southeast-1a.id,
      aws_subnet.privy-subnet-private2-ap-southeast-1b.id,
      aws_subnet.privy-subnet-public1-ap-southeast-1a.id,
      aws_subnet.privy-subnet-public2-ap-southeast-1b.id
    ]

    endpoint_private_access = false
    endpoint_public_access  = true
    public_access_cidrs     = ["202.162.39.186/32"]
  }

  depends_on = [aws_iam_role_policy_attachment.EKSPrivy-AmazonEKSClusterPolicy]
}
