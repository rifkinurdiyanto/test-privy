resource "aws_subnet" "privy-subnet-private1-ap-southeast-1a" {
  vpc_id            = aws_vpc.privy-test.id
  cidr_block        = "192.168.0.0/26"
  availability_zone = "ap-southeast-1a"

  tags = {
    "Name"                            = "privy-subnet-private1-ap-southeast-1a"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/EKSPrivy"  = "shared"
  }
}

resource "aws_subnet" "privy-subnet-private2-ap-southeast-1b" {
  vpc_id            = aws_vpc.privy-test.id
  cidr_block        = "192.168.0.64/26"
  availability_zone = "ap-southeast-1b"

  tags = {
    "Name"                            = "privy-subnet-private2-ap-southeast-1b"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/EKSPrivy"  = "shared"
  }
}

resource "aws_subnet" "privy-subnet-public1-ap-southeast-1a" {
  vpc_id                  = aws_vpc.privy-test.id
  cidr_block              = "192.168.0.128/26"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    "Name"                           = "privy-subnet-public1-ap-southeast-1a"
    "kubernetes.io/role/elb"         = "1"
    "kubernetes.io/cluster/EKSPrivy" = "shared"
  }
}

resource "aws_subnet" "privy-subnet-public2-ap-southeast-1b" {
  vpc_id                  = aws_vpc.privy-test.id
  cidr_block              = "192.168.0.192/26"
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    "Name"                           = "privy-subnet-public2-ap-southeast-1b"
    "kubernetes.io/role/elb"         = "1"
    "kubernetes.io/cluster/EKSPrivy" = "shared"
  }
}
