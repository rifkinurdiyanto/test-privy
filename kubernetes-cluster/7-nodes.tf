resource "aws_iam_role" "privy-nodes" {
  name = "node-group-nodes-EKSPrivy"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "privy-nodes-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.privy-nodes.name
}

resource "aws_iam_role_policy_attachment" "privy-nodes-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.privy-nodes.name
}

resource "aws_iam_role_policy_attachment" "privy-nodes-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.privy-nodes.name
}

resource "aws_eks_node_group" "privy-nodes" {
  cluster_name    = aws_eks_cluster.EKSPrivy.name
  node_group_name = "privy-nodes"
  node_role_arn   = aws_iam_role.privy-nodes.arn

  subnet_ids = [
    aws_subnet.privy-subnet-private1-ap-southeast-1a.id,
    aws_subnet.privy-subnet-private2-ap-southeast-1b.id
  ]

  capacity_type  = "ON_DEMAND"
  instance_types = ["t3.small"]
  ami_type       = "AL2_x86_64"
  disk_size      = 20

  scaling_config {
    desired_size = 1
    max_size     = 5
    min_size     = 0
  }

  update_config {
    max_unavailable = 1
  }

  labels = {
    role = "general"
  }

  remote_access {
    ec2_ssh_key = "EKSPrivyNodes"
  }

  depends_on = [
    aws_iam_role_policy_attachment.privy-nodes-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.privy-nodes-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.privy-nodes-AmazonEC2ContainerRegistryReadOnly,
  ]
}