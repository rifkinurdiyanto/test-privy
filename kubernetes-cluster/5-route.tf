resource "aws_route_table" "privy-private-route-table" {
  vpc_id = aws_vpc.privy-test.id

  tags = {
    Name = "privy-private-route-table"
  }
}

resource "aws_route_table" "privy-public-route-table" {
  vpc_id = aws_vpc.privy-test.id

  tags = {
    Name = "privy-public-route-table"
  }
}

resource "aws_route" "privy-private" {
  route_table_id         = aws_route_table.privy-private-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.privy-nat1.id
  depends_on             = [aws_route_table.privy-private-route-table]
}

resource "aws_route" "privy-public" {
  route_table_id         = aws_route_table.privy-public-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.privy-igw.id
  depends_on             = [aws_route_table.privy-public-route-table]
}

resource "aws_main_route_table_association" "privy-test" {
  vpc_id         = aws_vpc.privy-test.id
  route_table_id = aws_route_table.privy-private-route-table.id
}

resource "aws_route_table_association" "privy-subnet-private1-ap-southeast-1a" {
  subnet_id      = aws_subnet.privy-subnet-private1-ap-southeast-1a.id
  route_table_id = aws_route_table.privy-private-route-table.id
}

resource "aws_route_table_association" "privy-subnet-private2-ap-southeast-1b" {
  subnet_id      = aws_subnet.privy-subnet-private2-ap-southeast-1b.id
  route_table_id = aws_route_table.privy-private-route-table.id
}

resource "aws_route_table_association" "privy-subnet-public1-ap-southeast-1a" {
  subnet_id      = aws_subnet.privy-subnet-public1-ap-southeast-1a.id
  route_table_id = aws_route_table.privy-public-route-table.id
}

resource "aws_route_table_association" "privy-subnet-public2-ap-southeast-1b" {
  subnet_id      = aws_subnet.privy-subnet-public2-ap-southeast-1b.id
  route_table_id = aws_route_table.privy-public-route-table.id
}